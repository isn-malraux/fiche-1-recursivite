﻿# Créé par Vincent, le 17/01/2016 en Python 3.2

import turtle

def arbre(longueur):
    if longueur>10:
        turtle.forward(longueur)
        turtle.right(45)
        arbre(longueur//2)
        turtle.left(90)
        arbre(longueur//2)
        turtle.left(135)
        turtle.forward(longueur)
        turtle.left(180)

turtle.left(90)
arbre(200)
turtle.exitonclick()


