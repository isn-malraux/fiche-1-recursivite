﻿# Créé par Vincent, le 17/01/2016 en Python 3.2

import turtle

def ligne(longueur):
    if longueur<20:
        turtle.forward(longueur)
    else:
        ligne(longueur/3)
        turtle.left(60)
        ligne(longueur/3)
        turtle.right(120)
        ligne(longueur/3)
        turtle.left(60)
        ligne(longueur/3)

ligne(200)
turtle.exitonclick()



