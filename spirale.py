﻿# Créé par Vincent, le 17/01/2016 en Python 3.2

import turtle

def spirale(longueur):
    if longueur>20:
        turtle.forward(longueur)
        turtle.left(90)
        turtle.forward(longueur)
        turtle.left(90)
        spirale(longueur-20)

spirale(100)
turtle.exitonclick()



